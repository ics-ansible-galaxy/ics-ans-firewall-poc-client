import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('firewall_poc_client')


def test_malware_folder_exists(host):
    assert host.file("/etc/malware").exists


def test_curlformat_file_exists(host):
    assert host.file("/tmp/curl-format.txt").exists


def test_ca_bundle_trust_crt_file_exists(host):
    assert host.file("/etc/pki/tls/certs/ca-bundle.trust.crt").exists
